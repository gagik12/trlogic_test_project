<?php
define("ROOT_DIR", dirname(dirname(__FILE__)) . '/');

require_once "../vendor/Application.php";

$application = new Application();
$application->Run();