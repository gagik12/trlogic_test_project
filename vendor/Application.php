<?php
require_once "AutoLoader.php";

class Application
{
    public function Run(): void
    {
        spl_autoload_register(['AutoLoader', 'load'], true, true);
        try
        {
            $this->InitDBConnection();
        }
        catch (Exception $e)
        {
            echo '<h2>Error!</h2>'.
                '<h4>'.$e->getMessage().'</h4>'.
                '<pre>'.$e->getTraceAsString().'</pre>';
            exit;
        }
    }

    private function InitDBConnection(): void
    {
        $config = new ConfigDb();
    }
}