<?php

class AutoLoader
{
    const DIRECTORIES = [
        'apps',
        'config',
        'lib'
    ];

    /**
     * @param string $className
     * @throws Exception
     */
    public static function load(string $className)
    {
        self::find($className);

        if (!class_exists($className, false)
            && !interface_exists($className, false)
            && !trait_exists($className, false))
        {
            throw new Exception('Unable to find class: ' . $className);
        }
    }

    public static function find(string $className)
    {
        foreach (self::DIRECTORIES as $directory)
        {
            $filePath = ROOT_DIR . $directory . '/'. $className . ".php";
            if (is_readable($filePath))
            {
                require_once $filePath;
            }
        }
    }

}